package controller;


import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;

import repository.AccountRepository;
import repository.InDataBaseAccountRepository;
import repository.InMemoryAccountRepository;
import service.TransferService;
import domain.Account;

@Controller
public class TransController {

	@RequestMapping("/transferencia")
	String home(ModelMap model, @RequestParam String name ) {
		 model.addAttribute("name", name);			
	     return "transferencia";
	}
	
	@RequestMapping("/aprobacion")
	String operacion(ModelMap model, @RequestParam String cuenta1, @RequestParam String cuenta2, @RequestParam String monto)
	{
		
	   // AccountRepository repository = new InMemoryAccountRepository();
		AccountRepository repository1 = new InDataBaseAccountRepository();
		TransferService service = new TransferService(repository1);		
		
		Account a1 = new Account(cuenta1, 550);
		Account a2 = new Account(cuenta2, 50);		

		//repository.save(a1);
		//repository.save(a2);
		
		
	//	repository1.save(a1);
	//	repository1.save(a2);
		
		
		double amonto = Double.parseDouble(monto);
		if(service.transfer(cuenta1, cuenta2, amonto )){
			return "exito";
		}
		return "transferencia";
	}

}
