package controller;


import java.util.List;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;

import repository.AccountRepository;
import repository.InDataBaseAccountRepository;
import repository.InMemoryAccountRepository;
import service.TransferService;
import domain.Account;

@Controller
public class CuentasController {

	@RequestMapping("/transferencia")
	String home(ModelMap model, @RequestParam String name ) {
		 model.addAttribute("name", name);			
	     return "transferencia";
	}
	
	@RequestMapping("/aprobacion")
	String operacion(ModelMap model, @RequestParam String cuenta1, @RequestParam String cuenta2, @RequestParam String monto)
	{
		
	   // AccountRepository repository = new InMemoryAccountRepository();
		AccountRepository repository1 = new InDataBaseAccountRepository();
		TransferService service = new TransferService(repository1);		

		
		double amonto = Double.parseDouble(monto);
		if(service.transfer(cuenta1, cuenta2, amonto )){
			return "exito";
		}
		return "transferencia";
	}
	
	@RequestMapping("/cuentas")
	String cuentas(ModelMap model){
		AccountRepository repository1 = new InDataBaseAccountRepository();
		TransferService service = new TransferService(repository1);		
		
		model.addAttribute("lcuentas", service.findAll());
		
		return "cuentas";
	}

}
