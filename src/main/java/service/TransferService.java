package service;

import java.util.List;

import repository.AccountRepository;
import repository.InDataBaseAccountRepository;
import domain.Account;
public class TransferService {

	//AccountRepository repository = new InDataBaseAccountRepository();
	AccountRepository repository ;
	
	public TransferService(AccountRepository repository) {
	
		this.repository = repository;
		
	}

	public boolean transfer(String sourceNumber, String targetNumber, double amount) {
		
		
		Account sourceAccount = repository.findByNumber(sourceNumber);
		Account targetAccount = repository.findByNumber(targetNumber);
		
		
		if (sourceAccount.getNumber() == null || targetAccount.getNumber() == null) {
			System.err.println("No se puede realizar la transaccion");
			return false;
		}
		if (sourceAccount.getBalance() >= amount) {

			sourceAccount.setBalance(sourceAccount.getBalance() - amount);
			targetAccount.setBalance(targetAccount.getBalance() + amount);
			repository.update(sourceAccount);
			repository.update(targetAccount);
			return true;
		}
		return false;
		
	}
	public List<Account> findAll() {
		
		List<Account> cuentas = repository.findAll();
		
		System.err.println(cuentas.get(0).getNumber());
		return cuentas;
	}
}
