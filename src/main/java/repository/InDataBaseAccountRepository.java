package repository;

import domain.Account;
import UI.DataAccess;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.sql.*;

public class InDataBaseAccountRepository implements AccountRepository {

	Connection conn= null;
	

	@Override
	public Account findByNumber(String number) {
		// TODO Auto-generated method stub
		DataAccess Da = new DataAccess();
		Connection cnn = DataAccess.dbConnector();
	    try{

	    	String query = "Select * from cuentas where numero= '" + number+"'";    	    	
	    	PreparedStatement pst = cnn.prepareStatement(query);    		    	
	    	ResultSet resp = pst.executeQuery();    	
	    	
			String n = null;
			double d = 0;
	    	while(resp.next())
	    	{
	    		 n  = resp.getString("numero");
	    		 d = resp.getDouble("balance");
	    	}
			
	    	Account cuenta = new Account(n,d);
	    	cnn.close();
	    	return cuenta;
 
	    	
	    }catch(Exception e){
			System.err.println(e.getMessage());

			return null;

	    }
		
	}

	@Override
	public List<Account> findAll() {
		DataAccess Da = new DataAccess();
		Connection cnn = Da.dbConnector();
	    try{
	    	String query = "Select * from cuentas";
	    	PreparedStatement pst = cnn.prepareStatement(query);	    	
	    	ResultSet resp = pst.executeQuery();
	    	
	    	List<Account> lista =  new ArrayList<Account>();
	    	while(resp.next()){
	    		double amonto = Double.parseDouble(resp.getString(2));
		    	Account cuenta = new Account(resp.getString(1),amonto);
		    	lista.add(cuenta);	  		
	    	}   	
	    	
	    	cnn.close();
	    	return lista;
 
	    	
	    }catch(Exception e){
	    	
			return null;

	    }
		
	}

	@Override
	public Account save(Account account) {
		// TODO Auto-generated method stub
		DataAccess Da = new DataAccess();
		Connection cnn = DataAccess.dbConnector();
	    try{
	    	String monto = Double.toString(account.getBalance());	    	
	    	String query =  "INSERT INTO cuentas (numero,balance)  VALUES(?,?)";  	

	    	PreparedStatement pst = cnn.prepareStatement(query);	    
	    	pst.setString(1, account.getNumber());	    	
	    	pst.setString(2, monto);	 
	    	
	    	
	    	pst.executeUpdate();
	    	cnn.close();
			System.err.println("Se Inserto");

	    	return account;
 
	    	
	    }catch(Exception e){
			System.err.println(e.getMessage());

			return null;

	    }
	}

	@Override
	public Account remove(Account account) {
		// TODO Auto-generated method stub
		
		DataAccess Da = new DataAccess();
		Connection cnn = DataAccess.dbConnector();
	    try{
	    	
	    	String query =  "DELETE from COMPANY where numero=?"; 
	    	
	    	PreparedStatement pst = cnn.prepareStatement(query);	
	    	pst.setString(1, account.getNumber());
	    	
	    	pst.executeUpdate();
	    	
	    	cnn.close();
			System.err.println("Se Elimino");

	    	return account;
 
	    	
	    }catch(Exception e){
	    	System.err.println("No se elimino");

			return null;

	    }
	}

	@Override
	public Boolean update(Account account) {
		// TODO Auto-generated method stub
		DataAccess Da = new DataAccess();
		Connection cnn = DataAccess.dbConnector();
	    try{

	    	String query = "UPDATE cuentas set balance = "+ String.valueOf(account.getBalance()) +" where numero='"+account.getNumber()+"';";    	    	
	    	PreparedStatement pst = cnn.prepareStatement(query);    		    	
	    	pst.executeUpdate();    	
			System.err.println("Se actualizo " +account.getNumber() );


	    	return true;
	    }catch(Exception e)
	    {
			System.err.println(e.getMessage());

	    	return false;
	    }
		
	}
	
}
