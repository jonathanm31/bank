package UI;
import java.sql.*;

public class DataAccess {
	
	public static Connection dbConnector(){		
		
		 Connection conn = null;  
		try{
			Class.forName("org.sqlite.JDBC");  
			conn = DriverManager.getConnection("jdbc:sqlite:banco.sqlite");
			return conn;
			
		}catch(Exception e){
			return conn;
		}
	}
}
